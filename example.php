<?php
require __DIR__ . "/vendor/autoload.php";

use RUCD\Training\Trainer;
use RUCD\Training\TrainerParameters;
use RUCD\Training\SolutionDistance;
use RUCD\Training\SolutionAUC;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$populationSize = 100;
$crossoverRate = 60;
$mutationRate = 3;
$selectionMethod = TrainerParameters::SELECTION_METHOD_RWS;
$maxGeneration = 100;
$populationInitializationMethod = TrainerParameters::INITIAL_POPULATION_GENERATION_RANDOM;
$solutionType = new SolutionDistance(4);

// For logging you can use any implementation of PSR Logger
$logger = new Logger('wowa-training-test');
try {
    $logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));
} catch (Exception $e) {
}

$parameters = new TrainerParameters(
    $logger,
    $populationSize,
    $crossoverRate,
    $mutationRate,
    $selectionMethod,
    $maxGeneration,
    $populationInitializationMethod
);
$trainer = new Trainer($parameters, $solutionType);

// Input data
$data = [
  [0.1, 0.2, 0.3, 0.4],
  [0.1, 0.8, 0.3, 0.4],
  [0.2, 0.6, 0.3, 0.4],
  [0.1, 0.2, 0.5, 0.8],
  [0.5, 0.1, 0.2, 0.3],
  [0.1, 0.1, 0.1, 0.1],
];

// expected aggregated value for each data vector
$expected = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6];

var_dump($trainer->run($data, $expected));
