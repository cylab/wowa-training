\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{tabularx}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Multi-criteria decision system based with training applied to the detection of PHP webshells}

\author{\IEEEauthorblockN{Alexandre Croix}
\IEEEauthorblockA{\textit{Royal Military Academy}\\
Belgium \\
alexandre.croix@rma.ac.be}
\and
\IEEEauthorblockN{Thibault Debatty}
\IEEEauthorblockA{\textit{Royal Military Academy}\\
Belgium \\
thibault.debatty@rma.ac.be}
}

\maketitle

\begin{abstract}
In this paper we compare different optimization methods for learning Weighted Ordered Weighted Averaging (WOWA) coefficients in order to perform a binary classification. 
The learning part is based on a Genetic Algorithm and uses a dataset of examples. 
We perform a complete parameter study and we determine the efficiency of our model by evaluating the performance during the classification of different PHP files
as webshells or normal files. These PHP files were previously analyzed by a program developped at the Royal Military Academy. We obtain very accurate results and a good stability during the decision process.
This system could be used in a lot of different fields.
\end{abstract}

\begin{IEEEkeywords}
Webshell, machine learning, multi-criteria decision
\end{IEEEkeywords}

\section{Introduction}
Because of the growth of the amount of information, data fusion techniques, and aggregation functions especially, are more and more important. These techniques are used in a large field of knoweldge
and are connected at two main problems: (i) the characterization of a model and (ii) the determination of parameters for a known function.

Several approaches have been developed to determine parameters for a known function. Some of them used a large dataset of examples, and paramters are deducted from these examples. 
An advantage of this kind of determination method is that is possible to resolve coefficients without the presence of domain experts to supply crucial information. 
Futhermore, data may come from different locations and, currently, for the majory of research fields, it is easy to obtain data.

In this paper we focused on a method to determine parameters for the WOWA function. WOWA, for Weighted Ordered Weighted Averaging, is an aggregation operator introduced by Vicenç Torra in 1996\cite{wowa}. 
This operator is generalization o Ordered Weighted Averaging (OWA) and the Weighted mean. Concretely, WOWA merges a set of numerical data in a single number thanks to two weighing vectors: one for the weighted mean ($w$)
and the other for the OWA operator ($p$). The WOWA operator combines the advantages of both of them. The weighted mean, weights the information sources, and the OWA gives importance to the data according to their scores. 
The WOWA functions takes three vectors ($w$, $p$, $data$) in arguments to produce a single number. The expression could be represented by:
\begin{equation}
output = WOWA(w,p,data)   
\end{equation}

Section \ref{section:learning} describes the structure of our algorithm with an explanation of the variants we developped: (i) two different population initialization methods, (ii) two evaluation criteria and (iii) two selection methods. 
In Section \ref{section:parameterStudy} we perform a performance evaluation of our algorithm by studying the impact of different parameters on the classification of PHP files as webshells or as normal files. 
Numerical data for PHP files classification are provided by a webshell-detector program, developed at the Royal Military Academy. 
In Section \ref{section:futureWorks} we discuss about results and give some leads for improve the work in the future. And, finally, we giva our conclusion in Section \ref{section:conclusion}.

\section{Learning}
\label{section:learning}
Learning aggregatio operator weights from example is, usually, performed by an Active Set Method (ASM) \cite{AggregationLearning}. The WOWA operator, because it combines the weighted mean and the OWA operator, 
is not a quadratic minimization function and the complexity of ASM for WOWA is more complex than for others aggregation operators. 
\cite{ASMvsGA} gives information that, in case of WOWA operator, determine weight vectors has similar complexity with an ASM approach than with a Genetic Algorithm one.
Moreover, it is particullary difficult to implement an ASM algorithm for the WOWA operator. Solve this problem with a Genetic Algorithm is preferable.

A Genetic Algorithm is an evolutive procedure that maintains a population of individuals $P(t) = (x_{1}^{t},...,x_{n}^{t})$, for itération $t$. 
Each element of the population, called a "chromosome", is a potential solution to the problem. A chromosome is composed of different characteristics, named "genes". Each chromosome is evaluated to measure its performance. 
The following generation, $t+1$, is generated by keeping the best chromosomes from the generation $t$ and by reproducing them (crossover). Then, some chromosomes in the generation $t+1$ are randomly "mutated".
The process is repeated until the algorithm reaches a end condition.

We describe in this Section, all the steps of our algorithm: (i) the population initialization, followed by a loop composed of (ii) an evaluation, (iii) a selection step, (iv) a reproduction step and (v) the mutation step.

\subsection{Population initialization}
The initial population $P(0)$ is composed of $N$ chromosomes, each containing two vectors ($w$ and $p$) of $M$ genes, where $M$ is the number of numerical data to aggregate.
Chromosomes genes have value between 0 and 1 and must respect the following contraint: the sum of gene values in a weight vector is equal to 1.

\subsubsection{Random initilization}
The easiest method to do an initilization population is simply generate full random chromosomes. Practically, we generate a random number (between 0 and 1) for each genes. Then, we normalize the two weight vectors independently
to obtain chromosomes taht respect the constraint.
The following expressions are the equations used for the normalization: 
\begin{equation}
    w_{i}' = \frac{w_i}{\sum w_i}
\end{equation}

\begin{equation}
    p_{i}' = \frac{p_i}{\sum p_i}
\end{equation}
 
\subsubsection{Quasi-random initialization}
We implemented a "quasi-random" initilization method. A part of the initial population is, as previsouly, random, but another part is composed of particular chromosomes selected randomly ($x = (0,0,...,1,0,...0)$ for example).
The goal is to get a more uniform distribution of the population in the space. The Figure \ref{quasiRandom} illustrates a comparison between a random distribution and a quasi-random distribution.
\begin{figure}[htbp]
\centerline{\includegraphics[width=\linewidth]{Random_VS_Quasi_random_distribution.png}}
\caption{Figure shows 1000 points distributed (a) quasi-random distribution (additive subrandom numbers) and (b) random distribution in a two dimensionnal space}
\label{quasiRandom}
\end{figure}

\subsection{Chromosome evaluation performance}
For each generation, all chromosomes are individually evaluated in order to determine which ones give the best results. We implemented two different evaluation methods which have a quite different functionning.

\subsubsection{Distance performance evaluation}
The following pseudo-code from \cite{ASMvsAG} describes how works the distance evaluation method:

\begin{algorithm}
    \caption{Distance chromosome performance evaluation}
    \begin{algorithmic}
        \FOR{all chromosomes in population} 
        \STATE read $w$ and $p$ vectors from chromosome $i$
        \STATE $total\_distance = 0$
        \FOR{all examples in dataset}
        \STATE $real\_result \leftarrow$ read(example($j$) from dataset)
        \STATE $wowa\_output \leftarrow wowa(w,p,data(j))$
        \STATE $local\_dist \leftarrow real\_result - wowa\_output$
        \STATE $total\_dist \leftarrow total\_dist + (local\_dist)^{2}$
        \ENDFOR
        \STATE chromosome.performance $\leftarrow total\_distance$
        \ENDFOR
    \end{algorithmic}
\end{algorithm}

In other words, for each chromosome in the population, we compute the WOWA function on all the examples in the dataset. Then, we compute the difference between the WOWA result just cmputed and the result given 
in the dataset example. All these differences are added to obtain the total distance of a chromosome that is the performance of this individual. Lower is th distance, better is the chromosome.
Obtain a distance of zero means the algorithm found a combination of weights that match perfectly with all examples in the dataset.

\subsubsection{Area Under the Curve performance evaluation}
The Area Under the Curve (AUC) evaluation method is designed for binary classifications problems. In this case, for each chromosome in the population, we compute the WOWA function on all examples in the dataset and, then,
on these computed results, we perform the Area Under the CUrve evaluation thanks to a PHP module we implemented. This method gives directly an estimation of the classification efficiency for each chromosome. 
Unlike the previous method, higher is the result, better is the chromosome.

The following pseudo-code describes how works the AUC evaluation method:
\begin{algorithm}
    \caption{AUC chromosome performance evaluation}
    \begin{algorithmic}
        \FOR{all chromosomes in population}
        \STATE read $w$ and $p$ vectors from chromosome $i$
        \STATE $auc \leftarrow 0$
        \STATE $wowa\_output[]$
        \STATE $real\_result[]$
        \FOR{all examples in dataset}
        \STATE $real\_result[j] \leftarrow $ read example($j$) from dataset
        \STATE $wowa\_output[j] \leftarrow wowa(w, p$, example($j$))
        \ENDFOR
        \STATE $auc \leftarrow computeAUC(wowa\_output[], real\_result[])$
        \STATE chromosome.performance $\leftarrow auc$
        \ENDFOR
    \end{algorithmic}
\end{algorithm}

\subsection{Selection step}
In orer to create a new generation ($t+1$) of chromosomes, it is necessary to select some element from the generation $t$. The probability to select a chromosomes depends on its performance score evaluation.
We implemented two methods to select "parents" for the next generation.

The number of chromosomes selected as parents depends on the crosover parameter. The crossover is the percentage of the population kept from the generation $t$ to the generation $t+1$.

We also implented an eletism selection: the two best chromosomes (compare to their perofrmance evaluation score) from the generation $t$ are systematically kept unchanged in the generation $t+1$.

\cite{selectionMethod} describes different selection methods for Genetic Algorithm. In our work we used two of them : the Tournament Selection (TOS) and the Roulette Wheel Selection (RWS).
\newline

\subsubsection{Roulette Wheel Selection}
This selection method gives to each individual $i$ of the population a probability $p(i)$ of being selected proportional to its performance score $f(i)$. The expression of $p(i)$ is given by:
\begin{equation}
    p(i) = \frac{f(i)}{\sum_{j=1}^{n}f(j)}
\end{equation}
where $n$ represents the number of elements in the population.

The process is repeated, without replacement, untill obtain the required number of parents.

According to \cite{selectionMethod}, the Roulette Wheel Selection method could produce a premature convergence of the algorithm on a local minimum.
\newline

\subsubsection{Tournament Selection}
This method pick randomly two chromosomes and keep, for the next generation, the chromosome with the best performance score. Chromosomes with good high performance score have more chance to be selected for the next generation. 
As the previous selection method, the process is repeated until obtain the necessary number of parents.

\subsection{Reproduction step}
During the reproduction step, chromosomes which have been selected during selection, are combined two-by-two to create new individuals. The reproduction procedure is detailled below and is inspired from \cite{}.

First, a random number $\alpha$ is selected to determine the crosover point. This number is defined as:
\begin{equation}
    \alpha = \lceil(rnd*M)\rceil
\end{equation}
where $M$ is the number of numerical data to aggregate and $rnd$ is a random number between $0$ and $1$.

Two chromosomes are randomly selected:
\begin{equation}
   P_{dad} = (p_{d1}, p_{d2},...,p_{d\alpha},...,p_{dM}) 
\end{equation}
\begin{equation}
    P_{mom} = (p_{m1}, p_{m2},..., p_{m\alpha},...,p_{mM})
\end{equation}

The genes at the crossover point are combined together to create new genes:
\begin{equation}
    p_{new1} = p_{m\alpha} -\beta[p_{m\alpha} - p_{d\alpha}]
\end{equation}
\begin{equation}
    p_{new2} = p_{d\alpha} +\beta[p_{m\alpha} - p_{d\alpha}]
\end{equation}

where $\beta$ is a random number between $0$ and $1$.

The final step is the combination of the two parents and the two new genes to obtain two new chromosomes:
\begin{equation}
    child_{1} = (p_{m1},p_{m2},...,p_{new1},...,p_{dM})
\end{equation}

\begin{equation}
    child_2 = (p_{d1}, p_{d2},...,p_{new2},...,p_{mM})
\end{equation}

The reproduction procedure is repeated to fill the population.

\subsection{Mutation step}
Mutation step is important to avoid to converge too quickly on a local minimum. Indeed, without mutation, generation after generation, population will converge to a minimum. 
It is possible, even probable, that this minimum is a local minimum. Mutation produces  "jump" on another position in the space and can discover new areas fo the domain with better potential solutions.

Concretely, a random genes is selected and is replaced by a random value between $0$ and $1$. Then, the chromosome is normalized to respect the constraint.

\section{Parameters study and experimental evaluation}
\label{section:parameterStudy}
\subsection{Test setup}
To evaluate the efficiency of our algorithm, we performed parameters study on data provided by a webshell detector. The detector analyses PHP files according to five different modules and gives a score, between $0$ and $1$ for each 
of them. The different modules are:
\begin{itemize}
    \item \textbf{Signature} Check if the file is known in a signature database. This kind of detection works well for known malicious file;
     \item \textbf{Fuzzy hashing}
     \item \textbf{Dangerous routines} Check if the PHP file tries to execute some specific and potentially dangerous routines. For example: \textit{exec}, \textit{passthru}, \textit{system},...;
     \item \textbf{Obfuscation} Check if the file executes some functions like \textit{base64decode}, \textit{rot13},... A malicious file tries to hide the its content. The module measure also 
     the longest string in the file;
     \item \textbf{Entropy}
\end{itemize}
\newline

The dataset used for this work is composed of 12,468 PHP files that contains 206 PHP webshells. All these files were analyzed by the webshell-detector and the results were stored in serialized files.
\newline

The efficiency evaluation of a classification model needs two steps: (i) the learning part and (ii) the evaluation part. According to \cite{}, it is very important to have different dataset for the learning and the evaluation. 
Generally, two-third of the data are used for the learning and a third for the evaluation. 

In our case, it is not easy to find a sufficiant amount of real different webshells. To overcome this problem we used a well-known data-mining method: the \textit{k-folds crossover validation}. 
This method consists to separate the dataset in $k$ folds, performs the learning part on $k-1$ folds and then, evaluates the model on the last fold. The process is repeated $k$ times
by changing the fold used for the evaluation. All these $k$ intermediate results are meaned to obtain a general result. Take $k$ equals to 5 or 10 produce, usually, the best results.
\newline

Our dataset has only 1.6\% of malicous files. It is not possible to generate randomly the $k$ folds. There would be a too high probability to obtain very different repartition. A fold could be composed of only one or two webshells. 

Practically, we selected randomly, $\lfloor \frac{W}{k} \rfloor$ webshells, and $\lfloor \frac{F}{k}\rfloor$ "normal" files. Where $W$ is the total number of webshells in the dataset, 
$F$ is the total number of non-malicious files in the dataset and $k$ is the number of folds. We obtain $k$ folds with similar proportion of webshells.

To raise the penalty to do not detect a webshell file, we artificially increase the number of webshells in the learning dataset: concretelly, in each learning dataset, we put 10 times each webshell scores.
\newline

To determine the efficiency of our algorithm, we performed a complete parameters study. We tested: 
\begin{itemize}
    \item \textbf{Population size} number of individuals in each population;
    \item \textbf{Crossover rate} percentage of the population kept to generate the next generation;
    \item \textbf{Mutation rate} percentage of genes which are mutated in each generation;
    \item \textbf{Generation number} number of generation before stop the algorithm.
\end{itemize}
In combination to these four parameters, we have tested the two population initialization methods (random and quasi-random) and two performance evaluation criteria (distance evaluation and AUC evaluation).

To evaluate the performance of a parameters combination, we observed two measurements: (i) the average AUC obtained after the 10-folds cross validation and (ii) the range of treshold values
in which more than 95\% of the files are correctly classified. 

We measure this range in the fold that has the better AUC result and we compare to the range obtained if the aggregation is performed witha simple mean.

We obtained the best values for each parameters and we combined them to obtain the best model as possible.

\subsection{Population number}
For the first test, we varied the population number between 40 and 110 by step of 10.
The other parameters are fixed and their values are:
{\small
  \begin{tabularx}{\linewidth}{rl}
  \hline
  Crossover rate & $60$ \\
  Mutation rate & $15$ \\
  Generation number & $110$ \\ 
  \hline
  \end{tabularx}
}

The Figure \ref{fig:AUCPopulationNumber} presents the average AUC for all the values and for the four combinations of initialization and performance evaluation method.

\begin{figure}[htbp]
    \centerline{\includegraphics[width=\linewidth]{AUCPopulationNumber.png}}
    \caption{Figure shows the variation of average AUC according to the variation of population number for the four combinations of initiliaztion and performance evaluation method}
    \label{fig:AUCPopulationNumber}
\end{figure}


The Figures \ref{}, \ref{}, \ref{} and \ref{} show the size range in which more than 95\% of the files are correctly classified compared to the range size obtained with a classification performed thanks to a mean as aggregation.
It is important to note that, because of the random generation of the folds, the results depend of the model but also the composition of the folds. That can be observed on the Figure \ref{} in which there is a better range size
for the WOWA classification and the average classification.
\subsection{Crossover rate}
The second parameter studied is the crossover rate. The values varied from 10 to 90 by step of 10. The fixed values are:

{\small
  \begin{tabularx}{\linewidth}{rl}
  \hline
  Population size & $100$ \\
  Mutation rate & $15$ \\
  Generation number & $110$ \\ 
  \hline
  \end{tabularx}
}

As previsouly, the Figure \ref{fig:AUCCrossoverRate} shows the variation of the average AUC according to the crossover rate variation. 

\begin{figure}[htbp]
    \centerline{\includegraphics[width=\linewidth]{AUCCrossoverRate.png}}
    \caption{Figure shows the variation of average AUC according to the variation of crossover rate for the four combinations of initiliaztion and performance evaluation method}
    \label{fig:AUCCrossoverRate}
\end{figure}

The Figures \ref{}, \ref{}, \ref{} and \ref{} compare the range size detection between the WOWA classification and a mean classification.


\subsection{Mutation rate}
The third parameter is the mutation rate. We tested the values between 5 and 50 by step of 5. The other, fixed, values are:
{\small
  \begin{tabularx}{\linewidth}{rl}
  \hline
  Population size & $100$ \\
  Crossover rate & $60$ \\
  Generation number & $100$ \\
  \hline
  \end{tabularx}
}
On the Figure \ref{fig:AUCMutationRate} we can see the impact of the mutation rate variation.

\begin{figure}[htbp]
    \centerline{\includegraphics[width=\linewidth]{AUCMutationRate.png}}
    \caption{Figure shows the variation of average AUC according to the variation of mutation rate for the four combinations of initiliaztion and performance evaluation method}
    \label{fig:AUCMutationRate}
\end{figure}

On the Figures \ref{}, \ref{}, \ref{} and \ref{} we can observe the impact of the mutation rate on the size of the detection range, compare to the range size for a classification that uses a simple average as aggregation.

\subsection{Generation number}
The way the algorithm is built, the best element of a generation is equal or better (according to the performance evaluation criterion) than the best element of the previous generation. Increasing the generation number
can only improve the performance evaluation score. We use the learning curve to measure the evolution of the improvement. This curve represents the AUC value in function of the generation number.
Note that for the AUC performance criterion, the learning curve is always increasing. That is not the case for the distance performance evaluation criterion. Indeed, that is not because the distance decrease that the AUC 
increase.

We generated the learning curves with fixed parameters:
{\small
  \begin{tabularx}{\linewidth}{rl}
  \hline
  Population size & $100$ \\
  Crossover rate & $60$ \\
  Mutation rate & $15$ \\
  \hline
  \end{tabularx}
}

The Figures \ref{} and \ref{} are the learning curve generated for the combination random initialization/AUC performance criterion and the quasi-random initialization/AUC performance criterion.

The two other combinations (random initialization/distance performance criterion and quasi-random initialization/distance performance criterion) are shown on the Figures \ref{} and \ref{}. These figures show also the evolution
of the distance criterion (that is always decreasing).

\subsection{Synthesis results}
This parameters study highlight the best values for the different parameters (whereas they are independent). We can note some important things:
\begin{itemize}
    \item The best value of AUC is, obvisously, obtained with the AUC performance evaluation criterion;
    \item The bigger size of detection range are obtained with the distance performance evaluation criterion. That means the distance criterion is less sensitive than AUC criterion;
    \item The size of detection range, for AUC criterion, is more or less the same order of magnitude than the range size obtained with a classification obtained thanks to a mean aggregation of the scores;
\end{itemize}
One of the most important result observed, is the repartition of the weight values. The result changes significantly with the performance evaluation criterion. With the distance criterion, the score with the higher weight is,
usually, the Signature score (between 45\% to 90\%) followed by Dangerous routines or Entropy scores. The high weight for the Signature module seems logical. Indeed, if the signature is recognized by the system, the file 
is a PHP webshell without any doubt.
\newline

The AUC criterion gives very different results. In this case, the Signature module obtains the lowest weight (usually less than 5\%). The values of the other weights change a lot, no module dominates the others in a majority of tests.

\section{Future works}
\label{section:futureWorks}
There are a lot of different ways to continue this work or to improve it. Currently, if we want to add some elements in the dataset examples, it is necessary to perform again all the learning part. 
It could be very interresting for the system to be able to adapt itself by modifying the weight values without a complete recomputation. The deep-learning could be a interresting way to improve this point and, potentially, 
to obtain better results in sensitivity and AUC score.
\newline

Our method to determine the best combination of parameters, considers that all parameters are completely independent. It seems more realistic to consider a correlation between the different parameters. This point could be improve 
in a future work.
\newline

The algorithm is built to be very generic. It will be easy to implement other aggregation functions or performance evaluation criteria.

\section{Conclusion}
\label{section:conclusion}
Our work shows that is possible to implement a multi-criteria based decision able to learn on a dataset examples. We applied this algorithm on a realistic situation: classification of PHP files as a webshell or as a normal file.
We obtained very interresting results: between 98\% and 99\% of the files are correctly classified. In our opinion, the decreasing of the sensitivity of the system after the learning is the most interresting result.

The algorithm gives also important information about the modules that are aggregated. It highlights the modules with a high importance and, mostly, the less important. This could point out that a module is inefficient and improve it in priority.
\bibliographystyle{IEEEtran}
\bibliography{references.bib}

\end{document}
