<?php

namespace RUCD\Training;

class TrainingDataset
{
    //Two dimension array. Equivalent to the List<List<Double>> in Java code.
    public $data = [];
    public $expected = [];
    public $length;

    public function __construct(array $data = null, array $expected = null)
    {
        if ($data == null || $expected == null) {
        } elseif (count($data) != count($expected)) {
            throw new \Exception('Expected and data size must be equal');
        } else {
            $this->data = $data;
            $this->expected = $expected;
        }
        if ($this->expected == null) {
            $this->length = 0;
        } else {
            $this->length = count($expected);
        }
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function getExpected() : array
    {
        return $this->expected;
    }

    public function getLength() : int
    {
        return $this->length;
    }

    /*
     * $dataset in an array of TrainingDataset
     */
    public function addElementInDataset(TrainingDataset $dataset, int $index)
    {
        //Copy element from a dataset to another one in a specific position.
        $this->data[] = $dataset->data[$index];
        $this->expected[] = $dataset->expected[$index];
        $this->length++;
    }

    public function removeElementInDataset(int $index) : void
    {
        array_splice($this->data, $index, 1);
        array_splice($this->expected, $index, 1);
        $this->length--;
    }

    public function addFoldInDataset(array $data_to_add, int $index) : TrainingDataset
    {
        $this->data = array_merge($this->data, $data_to_add[$index]->data);
        $this->expected = array_merge($this->expected, $data_to_add[$index]->expected);
        $this->length += $data_to_add[$index]->length;
        return $this;
    }

    public function prepareFolds(int $fold_number) : array
    {
        $expected = $this->expected;
        $fold_dataset = []; //TrainingDataset array
        $alertNumber = floor(array_sum($expected) / $fold_number);
        $noAlertNumber = floor((count($expected) - array_sum($expected)) / $fold_number);

        for ($i = 0; $i < $fold_number; $i++) {
            $tmp_dataset = new TrainingDataset();
            $true_alert_counter = 0;
            $no_alert_counter = 0;
            while ($tmp_dataset->length < ($alertNumber + $noAlertNumber)) {
                $index = array_rand($this->expected);
                if ($this->expected[$index] == 1 && $true_alert_counter < $alertNumber) {
                    $tmp_dataset->addElementInDataset($this, $index);
                    $this->removeElementInDataset($index);
                    $true_alert_counter++;
                } elseif ($this->expected[$index] == 0 && $no_alert_counter < $noAlertNumber) {
                    $tmp_dataset->addElementInDataset($this, $index);
                    $this->removeElementInDataset($index);
                    $no_alert_counter++;
                }
            }
            $fold_dataset[] = $tmp_dataset;
        }
        $fold_counter = 0;
        while ($this->length > 0) {
            $i = array_rand($this->expected);
            $fold_dataset[$fold_counter]->addElementinDataset($this, $i);
            $this->removeElementInDataset($i);
            if ($fold_counter == count($fold_dataset) - 1) {
                $fold_counter = 0;
            } else {
                $fold_counter++;
            }
        }
        return $fold_dataset;
    }

    public function increaseTrueAlert(int $increase_ratio) : TrainingDataset
    {
        $data_size = count($this->expected);
        for ($i = 0; $i < $data_size; $i++) {
            if ($this->expected[$i] == 1) {
                for ($j = 0; $j < $increase_ratio - 1; $j++) {
                    $this->expected[] = $this->expected[$i];
                    $this->data[] = $this->data[$i];
                }
            }
        }

        return new TrainingDataset($this->data, $this->expected);
    }
}
