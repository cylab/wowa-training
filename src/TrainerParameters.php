<?php

namespace RUCD\Training;

use Psr\Log\LoggerInterface;

/**
 * Contains parameters for the WOWA trainer.
 * Allows to isolate the parameters and make them final...
 *
 * @author tibo
 */
class TrainerParameters
{

    const SELECTION_METHOD_RWS = 1;
    const SELECTION_METHOD_TOS = 2;

    const INITIAL_POPULATION_GENERATION_RANDOM = 1;
    const INITIAL_POPULATION_GENERATION_QUASI_RANDOM = 2;

    /**
     * Logger
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    private $populationSize;
    private $crossoverRate;
    private $mutationRate;
    private $selectionMethod;
    private $initializationMethod;

    // stop conditions
    private $maxGenerationNumber;
    private $triggerDistance;

    private $learningCurveFile;

    /**
     * Wraps the parameters of the trainer algorithm.
     * This way we are sure they cannot be modified during execution.
     * @param LoggerInterface $logger
     * @param int $populationSize
     * @param float $crossoverRate
     * @param float $mutationRate
     * @param int $selectionMethod
     * @param int $maxGenerationNumber
     * @param int $populationInitializationMethod
     * @param string|null $learningCurveFile
     */
    public function __construct(
        LoggerInterface $logger = null,
        int $populationSize = 100,
        float $crossoverRate = 60.0,
        float $mutationRate = 7.0,
        int $selectionMethod = self::SELECTION_METHOD_RWS,
        int $maxGenerationNumber = 90,
        int $populationInitializationMethod = self::INITIAL_POPULATION_GENERATION_QUASI_RANDOM,
        string $learningCurveFile = null
    ) {

        $this->logger = $logger;
        $this->setPopulationSize($populationSize);
        $this->setCrossoverRate($crossoverRate);
        $this->setMutationRate($mutationRate);
        $this->setSelectionMethod($selectionMethod);
        $this->setMaxGenerationNumber($maxGenerationNumber);
        $this->triggerDistance = 0.0001;
        $this->setInitialPopulationMethod($populationInitializationMethod);
        $this->learningCurveFile = $learningCurveFile;
    }

    /**
     * Setter for populationSize. Can't be negative number
     * @param int $populationSize
     *
     */
    private function setPopulationSize(int $populationSize)
    {
        if ($populationSize < 0) {
            if ($this->logger != null) {
                $this->logger->error(
                    "Population size must be a positive value. Entered value was : ",
                    $populationSize
                );
            }
            die("Population Size must be a positive value");
        }
        $this->populationSize = $populationSize;
    }

    /**
     * Setter for corssoverRate. Must between 0 and 100
     * @param float $crossoverRate
     */
    private function setCrossoverRate(float $crossoverRate)
    {
        if ($crossoverRate <= 0 || $crossoverRate >= 100) {
            if ($this->logger != null) {
                $this->logger->error(
                    "Crossover rate must be between 0 and 100 excluded. Value was  :",
                    $crossoverRate
                );
            }
            die("Crossover rate must be greater than 0 and lower than 100");
        }
        $this->crossoverRate = $crossoverRate;
    }

    /**
     * Setter for mutationRate. Must be greater than 0 and lower or equal than 100
     * @param float $mutation_rate
     */
    private function setMutationRate(float $mutation_rate) : void
    {
        if ($mutation_rate >= 100 || $mutation_rate < 0) {
            if ($this->logger != null) {
                $this->logger->error(
                    "Mutation rate must be positive and under 100.Entered value is : ",
                    $mutation_rate
                );
            }
            die("Mutation rate must be positive and under 100");
        }
        $this->mutationRate = $mutation_rate;
    }

    /**
     *
     * @param int $selectionMethod
     */
    private function setSelectionMethod(int $selectionMethod)
    {
        if ($selectionMethod == self::SELECTION_METHOD_RWS || $selectionMethod == self::SELECTION_METHOD_TOS) {
            $this->selectionMethod = $selectionMethod;
        } else {
            $this->logger->error("Selection method must be RWS or TOS");
            die("Selection method must be RWS or TOS");
        }
    }

    /**
     *
     * @param int $populationInitializationMethod
     */
    private function setInitialPopulationMethod(int $populationInitializationMethod)
    {
        if ($populationInitializationMethod == self::INITIAL_POPULATION_GENERATION_RANDOM ||
                $populationInitializationMethod == self::INITIAL_POPULATION_GENERATION_QUASI_RANDOM) {
            $this->initializationMethod = $populationInitializationMethod;
        } else {
            if ($this->logger != null) {
                $this->logger->error("Generation Initial Population must be RANDOM or QUASI_RANDOM");
            }
            die("Generation Initial Population must be RANDOM or QUASI_RANDOM");
        }
    }

    /**
     * Setter for the maximum number of generation
     * @param int $maxGenerationNumber
     */
    private function setMaxGenerationNumber(int $maxGenerationNumber)
    {
        if ($maxGenerationNumber < 10 || $maxGenerationNumber > 1000) {
            if ($this->logger != null) {
                $this->logger->error("Number of generation must be between 10 and 1000");
            }
            die("Number of generation must be be between 10 and 1000");
        }
        $this->maxGenerationNumber = $maxGenerationNumber;
    }

    /**
     * Getter for Initial Population Method
     * @return int
     */
    public function getInitialPopulationMethod() : int
    {
        return $this->initializationMethod;
    }
    /**
     * Getter for the maximum number of generation
     * @return int
     */
    public function getMaxGenerationNumber() : int
    {
        return $this->maxGenerationNumber;
    }

    public function getLogger()
    {
        return $this->logger;
    }

    public function getPopulationSize() : int
    {
        return $this->populationSize;
    }

    public function getCrossoverRate() : float
    {
        return $this->crossoverRate;
    }

    public function getMutationRate() : float
    {
        return $this->mutationRate;
    }

    public function getSelectionMethod() : int
    {
        return $this->selectionMethod;
    }

    public function getTriggerDistance()
    {
        return $this->triggerDistance;
    }


    /**
     * The number of parents required to perform reproduction.
     */
    public function getNumberParents() : float
    {
        $nbr_parents = round(
            ($this->getPopulationSize()
            * (1 - $this->getCrossoverRate() / 100))
        );

        if ($nbr_parents % 2 == 1) {
            $nbr_parents++;
        }

        return $nbr_parents;
    }
}
