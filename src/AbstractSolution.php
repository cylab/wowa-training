<?php

namespace RUCD\Training;

use Aggregation\WOWA;
use Cylab\ROC\ROC;
use Cylab\ROC\PRCurve;

/**
 * Represents one solution generated by the Trainer. Contains:
 * - w weights
 * - p weights
 * - distance between this solution and the target value used for training
 * @author alex
 */
abstract class AbstractSolution
{
    public $weights_w = [];
    public $weights_p = [];
    private $distance = INF;

    private $roc;

    private $auc_pr = null;

    public function __construct(int $number_of_weights)
    {
        for ($i = 0; $i < $number_of_weights; $i++) {
            $this->weights_w[$i] = Utils::getRandomDouble();
            $this->weights_p[$i] = Utils::getRandomDouble();
        }
        $this->normalize();
    }

    public function __toString()
    {
        return "Fitness score : " . abs($this->distance)
                . " w: [" . implode(", ", $this->weights_w) . "]"
                . " p: [" . implode(", ", $this->weights_p) . "]";
    }

    /**
     * Compute the distance between this solution and the provided pair
     * data, target_values
     * The distance is the sum of (aggregated_value - target_value)^2
     * @param array $data
     * @param array $target_values
     */
    public function computeDistanceTo(array $data, array $target_values)
    {
        $this->distance = $this->doComputeDistanceTo($data, $target_values);
    }

    abstract public function doComputeDistanceTo(array $data, array $target_values) : float;


    public function randomlyMutateWithProbability(float $probability) : void
    {
        $tos = $this->getRandomDouble();
        if ($tos > $probability) {
            // do nothing...
            return;
        }

        // new weight value and position
        $new_weight_value = $this->getRandomDouble();
        $new_weight_position = array_rand($this->weights_p);

        // Select w or p weights
        $weight_selection = mt_rand(0, 1);
        if ($weight_selection == 0) {
            $this->weights_p[$new_weight_position] = $new_weight_value;
        } else {
            $this->weights_w[$new_weight_position] = $new_weight_value;
        }

        $this->normalize();
    }

    /**
     * Normalize weights (make sure the sum of p weights = 1 and the sum of
     * w weights = 1).
     */
    public function normalize()
    {
        $this->weights_p = Utils::normalizeWeights($this->weights_p);
        $this->weights_w = Utils::normalizeWeights($this->weights_w);
    }

    /**
     * Sort (in place) an array of solutions (based on distance value).
     * @param array $solutions
     * @return bool true on success
     */
    public static function sort(array &$solutions)
    {
        return usort($solutions, function (AbstractSolution $a, AbstractSolution $b) {
            if ($a->distance < $b->distance) {
                return -1;
            } elseif ($a->distance > $b->distance) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    /**
     * Generate a random value between 0.0 and 1.0 (inclusive) using PHP
     * built in mt_rand() function.
     * @return double
     */
    public function getRandomDouble() : float
    {
        return mt_rand() / mt_getrandmax();
    }

    public function computeROC(array $data, array $expected)
    {
        $points = $this->computeWOWAForDataset($data);
        $this->roc = ROC::fromArrays($points, $expected);
    }

    public function computePRAUC(array $data, array $expected)
    {
        if (count($data) != count($expected)) {
            throw new \Exception("Data and expected must have the same size");
        }
        $score = $this->computeWOWAForDataset($data);
        $pr = PRCurve::byArray($score, $expected);
        $this->setAucPr($pr->computePRAUC());
    }

    protected function computeWOWAForDataset(array $data) : array
    {
        $score = [];
        for ($i = 0; $i < count($data); $i++) {
            $wowa = WOWA::wowa($this->weights_w, $this->weights_p, $data[$i]);
            $score[] = $wowa;
        }
        return $score;
    }

    /**
     * @return array
     */
    public function getWeightsW(): array
    {
        return $this->weights_w;
    }

    /**
     * @param array $weights_w
     */
    public function setWeightsW(array $weights_w): void
    {
        $this->weights_w = $weights_w;
    }

    /**
     * @return array
     */
    public function getWeightsP(): array
    {
        return $this->weights_p;
    }

    /**
     * @param array $weights_p
     */
    public function setWeightsP(array $weights_p): void
    {
        $this->weights_p = $weights_p;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Get the ROC computed from this solution.
     *
     * @return ROC
     */
    public function roc()
    {
        return $this->roc;
    }

    /**
     * @return null
     */
    public function getAucPr()
    {
        return $this->auc_pr;
    }

    /**
     * @param float $auc_pr
     */
    private function setAucPr(float $auc_pr): void
    {
        if ($auc_pr < 0.0 || $auc_pr > 1.0) {
            throw new \Exception("AUC must be between 0 and 1");
        }
        $this->auc_pr = $auc_pr;
    }
}
