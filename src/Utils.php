<?php

namespace RUCD\Training;

use Exception;

use Aggregation\WOWA;
use Cylab\ROC\Value;

/**
 * Class Utils. Some useful functions
 *
 * @file Utils
 * @source None
 * @package Training
 * @author Alexandre Croix <croix.alexandre@gmail.com>
 *
 */

class Utils
{

    /**
     * Normalize weights of a vector
     *
     * @param array  $weights. The vector with weights to normalize
     *
     * @return array The normalized weight vector
     */

    /*@var $weights array */
    public static function normalizeWeights(array $weights) : array
    {
        $sum_weight = array_sum($weights);

        //var_dump($weights);
        $weightsNormalized = array();
        for ($i = 0; $i < count($weights); $i++) {
            $weightsNormalized[$i] = $weights[$i] / $sum_weight;
        }
        return $weightsNormalized;
    }

    /**
     * Function to find the max distance of a population array
     * @param array $solutions Array used to find the max distance
     * @return double $max. Max distance
     */
    public static function findMaxDistance(array $solutions) : float
    {
        $max = -INF;
        foreach ($solutions as $solution) {
            if ($solution->getDistance() > $max) {
                $max = $solution->getDistance();
            }
        }
        return $max;
    }

    /**
     * Function to find the min distance of a population array
     * @param array $solutions Array used to find the min distance
     * @return double $min. Min distance
     */
    public static function findMinDistance(array $solutions) : float
    {
        $min = INF;
        foreach ($solutions as $solution) {
            if ($solution->getDistance() < $min) {
                $min = $solution->getDistance();
            }
        }
        return $min;
    }

    /**
     * @param array $solutions
     * @return float
     */
    public static function sumTotalDistance(array $solutions) : float
    {
        $sum = 0;
        foreach ($solutions as $solution) {
            $sum = $sum + $solution->getDistance();
        }
        return $sum;
    }

    /**
     * @param array $data
     * @param array $expected
     * @param AbstractSolution $solution
     * @return array
     * @throws Exception
     */
    public static function convertDataToPoints(array $data, array $expected, AbstractSolution $solution) : array
    {
        $points = [];
        if (count($data) != count($expected)) {
            throw new Exception("Data and Expected array have different sizes");
        }
        for ($i = 0; $i < count($expected); $i++) {
            $wowa = WOWA::wowa($solution->weights_w, $solution->weights_p, $data[$i]);
            $points[] = new Value($wowa, $expected[$i]);
        }
        return $points;
    }

    /**
     * Generate a random value between 0.0 and 1.0 (inclusive) using PHP
     * built in mt_rand() function.
     * @return double
     */
    public static function getRandomDouble() : float
    {
        return mt_rand() / mt_getrandmax();
    }

    public static function convertCSVToDataForTrainer(String $filename) : array
    {
        $data = [];
        $file = fopen($filename, 'r');
        while (($line = fgetcsv($file)) !== false) {
            $data[] = array_map('floatval', $line);
        }
        fclose($file);
        //return array_map('floatval', $data);
        return $data;
    }

    public static function convertCSVToExpectedForTrainer(string $filename) : array
    {
        $expected = [];
        $file = fopen($filename, 'r');
        while (($line = fgetcsv($file)) !== false) {
            $expected[] = floatval($line);
        }
        fclose($file);
        return $expected;
        //return array_map('floatval', $expected);
    }


    /**
     * @param array $data
     * @param array $expected
     * @param int $ratio
     * @throws Exception
     */
    public static function increaseTrueAlertNumber(array &$data, array &$expected, int $ratio) : void
    {
        if ($ratio <= 1) {
            throw new Exception("Ratio must be an int greater than 1");
        }
        $length = count($data);
        for ($i = 0; $i < $length; $i++) {
            if ($expected[$i] == 1) {
                for ($j = 0; $j < $ratio - 1; $j++) {
                    $data[] = $data[$i];
                    $expected[] = $expected[$i];
                }
            }
        }
    }

    public static function newPopulationTriggerComputation(int $generation_number) : int
    {
        if ($generation_number < 50) {
            return 10;
        }

        return (int) floor($generation_number / 5);
    }
}
