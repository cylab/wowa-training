<?php

namespace RUCD\Training;

/**
 *
 * Class GeneticAlgorithm. Learn weights for wowa based on GA
 *
 * @filesource Trainer
 * @category None
 * @package RUCD\Training
 * @author Alexandre Croix  <croix.alexandre@gmail.com>
 * @link https://gitlab.cylab.be/cylab/wowa-training
 *
 */
class Trainer
{

    // parameters (immutable)
    /* @var $parameters TrainerParameters */
    private $parameters;
    private $solutionType;


    /**
     * Instantiate a new trainer instance.
     * @param TrainerParameters $parameters
     * @param AbstractSolution $solution
     */
    public function __construct(TrainerParameters $parameters, AbstractSolution $solution)
    {
        $this->parameters = $parameters;
        $this->solutionType = $solution;
    }


    /**
     *
     * @param array $data
     * @param array $expected
     * @return AbstractSolution
     * @throws \Exception
     */
    public function run(array $data, array $expected) : AbstractSolution
    {
        $current_population = $this->generateInitialPopulationAndComputeDistances(
            $this->parameters->getPopulationSize(),
            $data,
            $expected
        );
        $best_solution = $this->findBestSolution($current_population);
        $generation_counter = 0;
        $new_population_trigger = Utils::newPopulationTriggerComputation($this->parameters->getMaxGenerationNumber());

        for ($generation = 0; $generation < $this->getParameters()->getMaxGenerationNumber(); $generation++) {
            $current_population = $this->performReproduction(
                $current_population,
                $data,
                $expected
            );

            $best_solution_of_current_population =
                    $this->findBestSolution($current_population);

            if ($this->getParameters()->getLogger() != null) {
                $this->getParameters()->getLogger()->debug(
                    "$generation | " . $best_solution_of_current_population
                );
            }

            // We found a new best solution
            if ($best_solution_of_current_population->getDistance()
                    < $best_solution->getDistance()) {
                $best_solution = $best_solution_of_current_population;
                $generation_counter = 0;
            } else {
                $generation_counter++;
                if ($generation_counter == $new_population_trigger) {
                    $this->parameters->getLogger()->info("New Half Population generated");
                    $this->solutionType::sort($current_population);
                    $half_population = array_slice(
                        $current_population,
                        0,
                        floor($this->parameters->getPopulationSize() / 2)
                    );
                    $new_half_population = $this->generateInitialPopulationAndComputeDistances(
                        $this->parameters->getPopulationSize() / 2,
                        $data,
                        $expected
                    );
                    $new_population = array_merge($half_population, $new_half_population);
                    $current_population = $new_population;
                    $generation_counter = 0;
                }
            }

            if (abs($best_solution->getDistance()) < $this->getParameters()->getTriggerDistance()) {
                break;
            }
        }

        return $best_solution;
    }

    public function runCSV(String $data_filename, String $expected_filename) : AbstractSolution
    {
        if ($this->getParameters()->getLogger() != null) {
            $this->getParameters()->getLogger()->debug("CSV data conversion");
            $data = Utils::convertCSVToDataForTrainer($data_filename);
            $this->getParameters()->getLogger()->debug("END CSV data conversion");
            $this->getParameters()->getLogger()->debug("CSV expected conversion");
            $expected = Utils::convertCSVToExpectedForTrainer($expected_filename);
            $this->getParameters()->getLogger()->debug("END CSV expected conversion");
        } else {
            $data = Utils::convertCSVToDataForTrainer($data_filename);
            $expected = Utils::convertCSVToExpectedForTrainer($expected_filename);
        }

        return $this->run($data, $expected);
    }

    public function runKFold(array $data, array $expected, int $fold_number) : array
    {

        $ds = new TrainingDataset($data, $expected);
        $folds = $ds->prepareFolds($fold_number);
        $solutions = [];
        for ($i = 0; $i < $fold_number; $i++) {
            $testing = $folds[$i];
            $learning = new TrainingDataset();
            for ($j = 0; $j < $fold_number; $j++) {
                if ($j != $i) {
                    $learning->addFoldInDataset($folds, $j);
                }
            }
            $sol = $this->run($data, $expected);
            $sol->computeROC($testing->data, $testing->expected);
            $solutions[] = $sol;
        }

        return $solutions;
    }

    public function runKFoldCSV(
        String $data_filename,
        String $expected_filename,
        int $fold_number
    ) : array {
        $data = Utils::convertCSVToDataForTrainer($data_filename);
        $expected = Utils::convertCSVToExpectedForTrainer($expected_filename);
        return $this->runKFold($data, $expected, $fold_number);
    }

    /**
     *
     * @param array $solutions
     * @return AbstractSolution
     */
    public function findBestSolution(array $solutions) : AbstractSolution
    {
        // The distance of a new Solution is INF
        $best_solution = new $this->solutionType(count($solutions[0]->getWeightsW()));

        /* @var $solution AbstractSolution */
        foreach ($solutions as $solution) {
            if ($solution->getDistance() < $best_solution->getDistance()) {
                $best_solution = $solution;
            }
        }

        return $best_solution;
    }

    /**
     * Function to generate a random population.
     * @param int $number_of_weights
     * @param int $population_size
     * @return array
     */
    public function generateInitialPopulation(int $number_of_weights, int $population_size) : array
    {
        $population = [];
        for ($i = 0; $i < $population_size; $i++) {
            $solution = new $this->solutionType($number_of_weights);
            $population[] = $solution;
        }
        return $population;
    }

    /**
     * Generate population
     * First part is random double
     * Second part is all vectors [0,0,1,0,0] combination types
     * @param int $number_of_weights
     * @param int $population_size must be equal or bigger than 2*(number_of_weights^2)
     * @return array
     */

    public function generateInitialPopulationWithForcedValues(int $number_of_weights, int $population_size) : array
    {
        $population = [];
        if ($population_size < 2 * pow($number_of_weights, 2)) {
            throw new \UnexpectedValueException("Population size too small for $number_of_weights weights");
        }
        for ($i = 0; $i < ($population_size - pow($number_of_weights, 2)); $i++) {
            $solution = new SolutionDistance($number_of_weights);
            for ($j = 0; $j < $number_of_weights; $j++) {
                $solution->getWeightsW()[$j] = $this->getRandomDouble();
                $solution->getWeightsP()[$j] = $this->getRandomDouble();
            }
            $solution->normalize();
            $population[] = $solution;
        }
        for ($k = 0; $k < $number_of_weights; $k++) {
            for ($l = 0; $l < $number_of_weights; $l++) {
                $solution = new $this->solutionType();
                $solution->weights_w = array_fill(0, $number_of_weights, 0);
                $solution->weights_p = array_fill(0, $number_of_weights, 0);
                $solution->weights_w[$k] = 1;
                $solution->weights_p[$l] = 1;
                $population[] = $solution;
            }
        }
        return $population;
    }


    /**
     * Generate population. First half is random double.
     * Second part of population is filled with [0,0,1,0,0] type randomly
     * @param int $number_of_weights
     * @param int $population_size
     * @return array
     */
    public function generateQuasiRandomInitialPopulation(int $number_of_weights, int $population_size) : array
    {
        $population = [];
        $number_of_random_element = $population_size / 2;
        if (pow($number_of_weights, 2) < $population_size / 2) {
            $number_of_random_element = $population_size - pow($number_of_weights, 2);
        }
        for ($i = 0; $i < $number_of_random_element; $i++) {
            $solution = new $this->solutionType($number_of_weights);
            $population[] = $solution;
        }
        $randomMemory = array();
        for ($k = 0; $k < $number_of_weights; $k++) {
            $randomMemory[$k] = array();
            for ($l = 0; $l < $number_of_weights; $l++) {
                $randomMemory[$k][$l] = 0;
            }
        }

        while (count($population) < $population_size) {
            $solution = new $this->solutionType($number_of_weights);
            $solution->weights_w = array_fill(0, $number_of_weights, 0);
            $solution->weights_p = array_fill(0, $number_of_weights, 0);
            $positionW = array_rand($solution->weights_w);
            $positionP = array_rand($solution->weights_p);
            if ($randomMemory[$positionW][$positionP] == 0) {
                $solution->weights_w[$positionW] = 1;
                $solution->weights_p[$positionP] = 1;
                $randomMemory[$positionW][$positionP] = 1;
                $population[] = $solution;
            }
        }
        return $population;
    }




    /**
     * Generate a random value between 0.0 and 1.0 (inclusive) using PHP
     * built in mt_rand() function.
     * @return double
     */
    public function getRandomDouble() : float
    {
        return mt_rand() / mt_getrandmax();
    }

    /**
     * For each solution, compute the value, and the distance compared to the target
     * values used for training.
     *
     * @param array $solutions
     * @param array $data
     * @param array $result
     * @return array solutions including computed value and distance
     */
    public function computeDistances(array $solutions, array $data, array $result) : array
    {
        /* @var $solution AbstractSolution */
        foreach ($solutions as $solution) {
            $solution->computeDistanceTo($data, $result);
        }

        return $solutions;
    }


    /**
     * Function to select chromosomes for next generation.
     * Based on roulette wheel selection
     * Each chromosome has a probability to be picked proportional to its score.
     *
     * @param array $solutions
     * @param int $count
     * @return array
     */
    private function rouletteWheelSelection(array $solutions, int $count) : array
    {

        $min = Utils::findMinDistance($solutions);
        $max = Utils::findMaxDistance($solutions);

        $selected = [];

        if (count($solutions) < $count) {
            throw new \UnexpectedValueException("Not enough solutions to select $count parents!");
        }

        while (count($selected) < $count) {
            $position = array_rand($solutions);
            $solution = $solutions[$position];

            // normalized is now a value in [0 - 1]
            $normalized_distance = ($solution->getDistance() - $min) / ($max - $min);

            // If distance is smaller, this solution has a higher probability
            // of being selected...
            $tos = Utils::getRandomDouble();
            if ($tos > $normalized_distance) {
                $selected[] = $solution;
                unset($solutions[$position]);
            }
        }

        return $selected;
    }

    /**
     * @param array $solutions
     * @param int $count
     * @return array
     */
    private function alternativeRouletteWheelSelection(array $solutions, int $count) : array
    {

        $min = Utils::findMinDistance($solutions);
        $max = Utils::findMaxDistance($solutions);
        $sum = Utils::sumTotalDistance($solutions);

        $selected = [];

        if (count($solutions) < $count) {
            throw new \UnexpectedValueException("Not enough solutions to select $count parents!");
        }

        while (count($selected) < $count) {
            $tos = Utils::getRandomDouble();
            $normalized_distance = 0;
            foreach ($solutions as $index => $solution) {
                $normalized_distance = $normalized_distance + ($max + $min - $solution->getDistance()) / $sum;
                if ($normalized_distance > $tos) {
                    $selected[] = $solution;
                    unset($solutions[$index]);
                    break;
                }
            }
        }

        return $selected;
    }


    /**
     * Function to select solutions based on tournament selection.
     * Random chromosomes are put together. Best distance score is selected.
     * @param array $solutions
     * @param int $count
     * @return array
     */
    private function tournamentSelection(array $solutions, int $count) : array
    {
        $selected = [];

        while (count($selected) < $count) {
            //$this->solutionType::sort($solutions);

            $solution1_position = array_rand($solutions);
            $solution1 = $solutions[$solution1_position];

            $solution2_position = array_rand($solutions);
            $solution2 = $solutions[$solution2_position];

            if ($solution1->getDistance() < $solution2->getDistance()) {
                $selected[] = $solution1;
                unset($solutions[$solution1_position]);
            } else {
                $selected[] = $solution2;
                unset($solutions[$solution2_position]);
            }
        }

        return $selected;
    }


    /**
     * Select parent solutions that will be used for reproduction.
     * The 2 best current solutions are selected, plus other according to
     * chosen selection method.
     * @param array $solutions Chromosomes with computed distance
     * @param int $count
     * @param int $method
     * @return array
     */
    public function selectParents(array $solutions, int $count, int $method) : array
    {
        $selected_parents = array();

        // Select the 2 best current solutions:
        // 1. sort
        $this->solutionType::sort($solutions);
        // 2. shift two elements (remove first element of array)
        $selected_parents[] = array_shift($solutions);
        $selected_parents[] = array_shift($solutions);

        if ($method == TrainerParameters::SELECTION_METHOD_TOS) {
            return array_merge(
                $selected_parents,
                $this->tournamentSelection($solutions, $count - 2)
            );
        }

        if ($method == TrainerParameters::SELECTION_METHOD_RWS) {
            return array_merge(
                $selected_parents,
                $this->alternativeRouletteWheelSelection($solutions, $count - 2)
            );
        }

        throw new \UnexpectedValueException("Invalid selection method");
    }

    /**
     * Function to initialize reproduction with crossover
     * Method use for crossover : See
     * https://gitlab.cylab.be/cylab/wowa-training/blob/master/doc/TheContinuousGeneticAlgorithm.pdf p59
     * @param array $solutions
     * @return array
     */
    public function doReproduction(array $solutions) : array
    {
        /* @var $solutions AbstractSolution[] */
        $nbr_weights = count($solutions[0]->getWeightsP());
        $original_population_size = count($solutions);

        // we add children to the current list of solutions
        while (count($solutions) < $this->getParameters()->getPopulationSize()) {
            $dad = rand(0, $original_population_size - 1);
            do {
                $mom = rand(0, $original_population_size - 1);
            } while ($dad == $mom);

            $cut_position = mt_rand(0, $nbr_weights - 1);

            // beta is used to compute the new value at the cut position
            $beta = Utils::getRandomDouble();

            $solutions = array_merge(
                $solutions,
                $this->reproduce($solutions[$dad], $solutions[$mom], $cut_position, $beta)
            );
        }

        while (count($solutions) > $this->getParameters()->getPopulationSize()) {
            array_pop($solutions);
        }

        return $solutions;
    }

    /**
     * Compute the reproduction of dad and mom, by cutting weights at
     * $cut_position and using beta to compute the value of the weight at
     * $cut_position
     * Returns an array containing 2 child solutions
     * @param AbstractSolution $dad
     * @param AbstractSolution $mom
     * @param int $cut_position
     * @param float $beta
     * @return AbstractSolution[]
     */
    public function reproduce(AbstractSolution $dad, AbstractSolution $mom, int $cut_position, float $beta) : array
    {

        // Weights at cut position;
        $pnew1W = $dad->weights_w[$cut_position]
                - $beta * ($dad->weights_w[$cut_position] - $mom->weights_w[$cut_position]);
        $pnew2W = $mom->weights_w[$cut_position]
                + $beta * ($dad->weights_w[$cut_position] - $mom->weights_w[$cut_position]);

        $pnew1P = $dad->weights_p[$cut_position]
                - $beta * ($dad->weights_p[$cut_position] - $mom->weights_p[$cut_position]);
        $pnew2P = $mom->weights_p[$cut_position]
                + $beta * ($dad->weights_p[$cut_position] - $mom->weights_p[$cut_position]);

        $child1 = new $this->solutionType(count($dad->weights_w));
        $child2 = new $this->solutionType(count($dad->weights_w));
        for ($i = 0; $i < $cut_position; $i++) {
            $child1->weights_w[$i] = $dad->weights_w[$i];
            $child1->weights_p[$i] = $dad->weights_p[$i];

            $child2->weights_w[$i] = $mom->weights_w[$i];
            $child2->weights_p[$i] = $mom->weights_p[$i];
        }

        $child1->weights_w[$cut_position] = $pnew1W;
        $child2->weights_w[$cut_position] = $pnew2W;
        $child1->weights_p[$cut_position] = $pnew1P;
        $child2->weights_p[$cut_position] = $pnew2P;

        $number_of_weights = count($dad->weights_p);
        for ($i = $cut_position + 1; $i < $number_of_weights; $i++) {
            $child1->weights_w[$i] = $mom->weights_w[$i];
            $child1->weights_p[$i] = $mom->weights_p[$i];

            $child2->weights_w[$i] = $dad->weights_w[$i];
            $child2->weights_p[$i] = $dad->weights_p[$i];
        }
        //only if we used a QuasiRandomGeneration
        if ($this->parameters->getInitialPopulationMethod() ==
                TrainerParameters::INITIAL_POPULATION_GENERATION_QUASI_RANDOM) {
            $this->checkAndCorrectNullWeightsVector($child1);
            $this->checkAndCorrectNullWeightsVector($child2);
        }

        $child1->normalize();
        $child2->normalize();
        return [$child1, $child2];
    }

    /**
     * Function for mutation genes. Change randomly some genes.
     * Except the 2 best from previous generation
     * @param array $solutions which mutation were done
     * @return array
     */
    public function randomlyMutateGenes(array $solutions) : array
    {
        $probability = $this->getParameters()->getMutationRate() / 100.0;

        /* @var $solution AbstractSolution */
        foreach ($solutions as $key => $solution) {
            if ($key > 1) {
                $solution->randomlyMutateWithProbability($probability);
            }
        }

        return $solutions;
    }

    /**
     *
     * @return TrainerParameters
     */
    public function getParameters() : TrainerParameters
    {
        return $this->parameters;
    }

    /**
     *
     * @param int $population_size
     * @param array $data
     * @param array $expected
     * @return array
     */
    public function generateInitialPopulationAndComputeDistances(
        int $population_size,
        array$data,
        array $expected
    ) : array {

        $number_of_weights = count($data[0]);
        $initial_population = [];
        if ($this->getParameters()->getInitialPopulationMethod() ==
                TrainerParameters::INITIAL_POPULATION_GENERATION_RANDOM) {
            $initial_population = $this->generateInitialPopulation(
                $number_of_weights,
                $population_size
            );
        } elseif ($this->getParameters()->getInitialPopulationMethod() ==
                TrainerParameters::INITIAL_POPULATION_GENERATION_QUASI_RANDOM) {
            $initial_population = $this->generateQuasiRandomInitialPopulation(
                $number_of_weights,
                $population_size
            );
        }

        return $this->computeDistances(
            $initial_population,
            $data,
            $expected
        );
    }

    /**
     * Select some parents from the population, let them reproduce, and add
     * some random mutations...
     *
     * @param array $population
     * @param array $data
     * @param array $expected
     * @return array
     */
    public function performReproduction(array $population, array $data, array $expected) : array
    {
        $parents = $this->selectParents(
            $population,
            $this->getParameters()->getNumberParents(),
            $this->getParameters()->getSelectionMethod()
        );

        $new_generation = $this->doReproduction($parents);
        $mutated = $this->randomlyMutateGenes($new_generation);

        return $this->computeDistances(
            $mutated,
            $data,
            $expected
        );
    }

    /**
     * @param AbstractSolution $child
     */
    private function checkAndCorrectNullWeightsVector(AbstractSolution $child) : void
    {
        if (array_sum($child->weights_w) == 0) {
            $position = array_rand($child->weights_w);
            $child->weights_w[$position] = 1;
        }
        if (array_sum($child->weights_p) == 0) {
            $position = array_rand($child->weights_p);
            $child->weights_p[$position] = 1;
        }
        return;
    }

    /**
     * @return AbstractSolution
     */

    public function getSolutionType()
    {
        return $this->solutionType;
    }
}
