<?php

/**
 * Generate random data, expected values, and random solutions.
 */

require __DIR__ . "/../vendor/autoload.php";

use \RUCD\Training\Trainer;
use \RUCD\Training\Utils;
use Aggregation\WOWA;
use \RUCD\Training\TrainerParameters;

$count = 10000;
$vector_length = 10;

$data = [];
$expected = [];
$weightW = [];
$weightP = [];

for ($k = 0; $k < $vector_length; $k++) {
    $weightW[] = mt_rand();
    $weightP[] = mt_rand();
}

$weightW = RUCD\Training\Utils::normalizeWeights($weightW);
$weightP = RUCD\Training\Utils::normalizeWeights($weightP);


for ($i = 0; $i < $count; $i++) {
    $vector = [];
    for ($j = 0; $j < $vector_length; $j++) {
        $vector[] = mt_rand() / mt_getrandmax();
    }
    $data[] = $vector;
    $expected[] = WOWA::wowa($weightW, $weightP, $vector);
}


$tmp_data = tempnam(sys_get_temp_dir(), "wowa_data_");
file_put_contents($tmp_data, serialize($data));
echo "data written to $tmp_data\n";

$tmp_expected = tempnam(sys_get_temp_dir(), "wowa_expected_");
file_put_contents($tmp_expected, serialize($expected));
echo "target values (expected) written to $tmp_expected\n";

$tmp_weight_w = tempnam(sys_get_temp_dir(), "wowa_weight_w_");
file_put_contents($tmp_weight_w, serialize($weightW));
echo "Weight W values written to $tmp_weight_w \n";

$tmp_weight_p = tempnam(sys_get_temp_dir(), "wowa_weight_p_");
file_put_contents($tmp_weight_p, serialize($weightP));
echo "Weight P values written to $tmp_weight_p \n";

$solution = new \RUCD\Training\SolutionDistance(sizeof($weightP));
$trainer = new Trainer(null, new TrainerParameters($solution));
$solutions = $trainer->generateInitialPopulationAndComputeDistances(
    100,
    $data,
    $expected
);


$tmp_solutions = tempnam(sys_get_temp_dir(), "wowa_solutions_");
file_put_contents($tmp_solutions, serialize($solutions));
echo "100 random solutions written to $tmp_solutions\n";
