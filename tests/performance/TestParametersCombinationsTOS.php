<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace RUCD\Training;

require __DIR__ . "/../../vendor/autoload.php";

//Parameters

$populationSize = 110;
$mutationRate = 17;
$crossoverRate = 30;
$generationNumber = 80;

$numberOfIterations = 10;

$data = unserialize(file_get_contents(__DIR__ . "/../DataTraining/data_webshell.ser"));
$expected = unserialize(file_get_contents(__DIR__ . "/../DataTraining/expected_webshell.ser"));
$meanValue = 0;

while ($populationSize <= 170) {
    while ($mutationRate <= 23) {
        while ($crossoverRate <= 60) {
            while ($generationNumber <= 120) {
                $parameters = new TrainerParameters(
                    null,
                    $populationSize,
                    $crossoverRate,
                    $mutationRate,
                    TrainerParameters::SELECTION_METHOD_TOS,
                    $generationNumber
                );
                for ($i = 0; $i < $numberOfIterations; $i++) {
                    $trainer = new Trainer($parameters, sizeof(sizeof($data)));
                    $distance = $trainer->run($data, $expected);
                    $info = "Population : $populationSize - "
                        . "Mutationrate : $mutationRate, "
                        . "crossoverRate : $crossoverRate, "
                        . "NumberOfGeneration : $generationNumber. Result : ";
                    $contents = $info . $distance->getDistance() . "\n";
                    file_put_contents('Performance_combinationsTOS.txt', $contents, FILE_APPEND);
                    $meanValue = $meanValue + $distance->getDistance();
                    echo "$contents";
                }
                file_put_contents(
                    'Performance_combinationsTOS.txt',
                    "MEAN VALUE : ".$meanValue / $numberOfIterations ."\n",
                    FILE_APPEND
                );
                echo "Mean Value :" .$meanValue / $numberOfIterations ." \n";
                $meanValue = 0;
                $generationNumber += 10;
            }
            $meanValue = 0;
            $crossoverRate += 5;
            $generationNumber = 70;
        }
        $meanValue = 0;
        $mutationRate += 1;
        $crossoverRate = 25;
    }
    $meanValue = 0;
    $populationSize += 10;
    $mutationRate = 15;
}
