<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace RUCD\Training;

require __DIR__ . "/../../vendor/autoload.php";

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$populationSize = $argv[1];
$crossoverRate = $argv[2];
$mutationRate = $argv[3];
$maxGeneration = $argv[4];
$generationInitialPopulationMethod = $argv[5];
$dataFileName = $argv[6];
$expectedFileName = $argv[7];
$foldNumber = $argv[8];
$solutionType = $argv[9];
$learningCurve = null;

if ($generationInitialPopulationMethod == 'RANDOM') {
    $generationInitialPopulationMethod = TrainerParameters::INITIAL_POPULATION_GENERATION_RANDOM;
} elseif ($generationInitialPopulationMethod == 'QUASI_RANDOM') {
    $generationInitialPopulationMethod = TrainerParameters::INITIAL_POPULATION_GENERATION_QUASI_RANDOM;
}

if ($solutionType == 'distance') {
    $solutionType = new SolutionDistance(5);
} elseif ($solutionType == 'auc') {
    $solutionType = new SolutionAUC(5);
}

$data = unserialize(file_get_contents($dataFileName));
$expected = unserialize(file_get_contents($expectedFileName));


echo count($expected) . " files analyzed\n";
echo array_sum($expected) . " malicious files\n";

$logger = new Logger('wowa-training-test');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::DEBUG));
$start = microtime(true);
$parameters = new TrainerParameters(
    $logger,
    $populationSize,
    $crossoverRate,
    $mutationRate,
    TrainerParameters::SELECTION_METHOD_RWS,
    $maxGeneration,
    $generationInitialPopulationMethod,
    $learningCurve
);
$trainer = new Trainer($parameters, $solutionType);
echo "Beginning of trainning \n";

if ($foldNumber == 1) {
    $start_time = microtime(true);
    $result = $trainer->run($data, $expected);
    $elepased_time = microtime(true) - $start_time;
} else {
    $start_time = microtime(true);
    $result = $trainer->runKFold($data, $expected, $foldNumber);
    $elepased_time = microtime(true) - $start_time;
}

var_dump($result);

$averageScore = 0;
foreach ($result as $key => $fold) {
    $averageScore = $averageScore + $fold['roc'];
}

$averageScore = $averageScore / $foldNumber;
echo "Average score : $averageScore \n";

$logger->debug("Execution time : " . $elepased_time . " seconds");

//$file = fopen('SimpleTest.csv', 'w');
//fputcsv($file, array('Wowa', 'Average', 'Expected'));
//for ($i = 0; $i < count($data); $i++) {
//    $wowa = WOWA::wowa($result->weights_w, $result->weights_p, $data[$i]);
//    $average = array_sum($data[$i]) / count($data[$i]);
//    fputcsv(
//        $file,
//        array($wowa, $average, $expected[$i])
//    );
//}


//Utils::computeStatisticalInformation($data, $expected, 50, 'Statistical.txt', $result);

/*
$weight_w = [0.1, 0.2, 0.3, 0.1, 0.3];
$weight_p = [0.3, 0.2, 0.2, 0.1, 0.2];

$data = [];
for ($i = 0; $i < 50; $i++) {
    $elements = [];
    for ($j = 0; $j < 5; $j++) {
        $elements[$j] = mt_rand() / mt_getrandmax();
    }
    $data[$i] = $elements;
}
var_dump($data);

$expected = [];
for ($i = 0; $i < 50; $i++) {
    $expected[$i] = WOWA::wowa($weight_w, $weight_p, $data[$i]);
}
$logger = new Logger('wowa-training-test');
$parameters = new TrainerParameters($logger);

$trainer = new Trainer($parameters, new SolutionDistance());
$solutions = $trainer->run($data, $expected);
var_dump($solutions);
 */
