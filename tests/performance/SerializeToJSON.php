<?php

namespace RUCD\Training;

require __DIR__ . "/../../vendor/autoload.php";

$data = unserialize(file_get_contents("/home/alex/Documents/wowa-training/tests/webshell_data.ser"));
$expected = unserialize(file_get_contents("/home/alex/Documents/wowa-training/tests/webshell_expected.ser"));

$dataJson = json_encode($data);
$expectedJson = json_encode($expected);

file_put_contents("/home/alex/Documents/java-wowa-training/ressources/webshell_data_new_version.json", $dataJson);
file_put_contents(
    "/home/alex/Documents/java-wowa-training/ressources/webshell_expected_new_version.json",
    $expectedJson
);


$text = file_get_contents("/home/alex/Documents/java-wowa-training/ressources/webshell_expected_new_version.json");
$formatted = implode(',', str_split($text));
file_put_contents("/home/alex/Documents/java-wowa-training/ressources/webshell_expected_formatted.json", $formatted);
