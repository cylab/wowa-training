<?php

namespace RUCD\unit;

use RUCD\Training\TrainingDataset;
use PHPUnit\Framework\TestCase;
use RUCD\Training\Utils;

class TrainingDatasetTest extends TestCase
{
    protected $data_set1;
    protected $data_set2;
    protected $data_set_small;

    protected function setUp()
    {
        $data1 = array_slice(Utils::convertCSVToDataForTrainer(__DIR__ . "/../resources/webshell_data.csv"), 0, 200);
        $expected1 = array_slice(
            Utils::convertCSVToExpectedForTrainer(__DIR__ . "/../resources/webshell_expected.csv"),
            0,
            200
        );
        try {
            $this->data_set1 = new TrainingDataset($data1, $expected1);
        } catch (\Exception $e) {
        }

        $data2 = array_slice(Utils::convertCSVToDataForTrainer(__DIR__ . "/../resources/webshell_data.csv"), 200, 200);
        $expected2 = array_slice(
            Utils::convertCSVToExpectedForTrainer(__DIR__ . "/../resources/webshell_expected.csv"),
            200,
            200
        );
        try {
            $this->data_set2 = new TrainingDataset($data2, $expected2);
        } catch (\Exception $e) {
        }

        $data_small
            = array_slice(Utils::convertCSVToDataForTrainer(__DIR__ . "/../resources/webshell_data.csv"), 200, 10);
        $expected_small = array_slice(
            Utils::convertCSVToExpectedForTrainer(__DIR__ . "/../resources/webshell_expected.csv"),
            200,
            10
        );
        try {
            $this->data_set_small = new TrainingDataset($data_small, $expected_small);
        } catch (\Exception $e) {
        }
    }

    public function testAddElementInDataset()
    {
        for ($i = 0; $i < $this->data_set_small->length; $i++) {
            $this->data_set1->addElementInDataset($this->data_set_small, $i);
        }
        $data_assertion
            = array_slice(Utils::convertCSVToDataForTrainer(__DIR__ . "/../resources/webshell_data.csv"), 0, 210);

        $expected_assertion = array_slice(
            Utils::convertCSVToExpectedForTrainer(__DIR__ . '/../resources/webshell_expected.csv'),
            0,
            210
        );
        $assertion = new TrainingDataset($data_assertion, $expected_assertion);

        self::assertEquals(count($data_assertion), $this->data_set1->length);
        self::assertEquals($data_assertion, $this->data_set1->data);
        self::assertEquals($expected_assertion, $this->data_set1->expected);
        self::assertEquals($assertion, $this->data_set1);
    }

    public function testRemoveElementInDataset()
    {
        for ($i = 199; $i >= 190; $i--) {
            $this->data_set1->removeElementInDataset($i);
        }

        $data_assertion = array_slice(
            Utils::convertCSVToDataForTrainer(__DIR__ . "/../resources/webshell_data.csv"),
            0,
            190
        );
        $expected_assertion = array_slice(
            Utils::convertCSVToExpectedForTrainer(__DIR__ . "/../resources/webshell_expected.csv"),
            0,
            190
        );

        $assertion = new TrainingDataset($data_assertion, $expected_assertion);

        self::assertEquals($assertion->length, $this->data_set1->length);
        self::assertEquals($assertion->data, $this->data_set1->data);
        self::assertEquals($assertion->expected, $this->data_set1->expected);

        $data_assertion = array_slice(
            Utils::convertCSVToDataForTrainer(__DIR__ . "/../resources/webshell_data.csv"),
            210,
            190
        );
        $expected_assertion = array_slice(
            Utils::convertCSVToExpectedForTrainer(__DIR__ . "/../resources/webshell_expected.csv"),
            210,
            190
        );

        $assertion = new TrainingDataset($data_assertion, $expected_assertion);

        while ($this->data_set2->length > 190) {
            $this->data_set2->removeElementInDataset(0);
        }
        self::assertEquals($assertion->length, $this->data_set2->length);
        self::assertEquals($assertion->data, $this->data_set2->data);
        self::assertEquals($assertion->expected, $this->data_set2->expected);
    }

    public function testAddFoldInDataset()
    {
        $list_dataset = [];
        $list_dataset[] = $this->data_set2;
        $list_dataset[] = $this->data_set_small;
        $this->data_set1->addFoldInDataset($list_dataset, 0);

        $data_assertion = array_slice(
            Utils::convertCSVToDataForTrainer(__DIR__ . "/../resources/webshell_data.csv"),
            0,
            400
        );
        $expected_assertion = array_slice(
            Utils::convertCSVToExpectedForTrainer(__DIR__ . "/../resources/webshell_expected.csv"),
            0,
            400
        );

        $assertion = new TrainingDataset($data_assertion, $expected_assertion);

        self::assertEquals($assertion->length, $this->data_set1->length);
        self::assertEquals($assertion->data, $this->data_set1->data);
        self::assertEquals($assertion->expected, $this->data_set1->expected);
    }

    public function testIncreaseTrueAlert()
    {
        $data = $this->generateData(100, 5);
        $original_data_size = count($data);
        $expected = $this->generateExpected(100);
        $original_expected_size = count($expected);
        $increase_ratio = 10;
        $number_true_alert = array_sum($expected);
        $number_no_alert = $original_data_size - $number_true_alert;

        $ds = new TrainingDataset($data, $expected);

        $ds_increased = $ds->increaseTrueAlert($increase_ratio);

        self::assertEquals($number_true_alert * $increase_ratio + $number_no_alert, $ds_increased->length);
        self::assertEquals($number_true_alert * $increase_ratio, array_sum($ds_increased->expected));

        for ($i = 0; $i < $ds->length; $i++) {
            if ($ds_increased->expected[$i] == 1.0) {
                $cnt = 0;
                for ($j = 0; $j < $ds_increased->length; $j++) {
                    if ($ds_increased->data[$i] === $ds_increased->data[$j]) {
                        $cnt++;
                    }
                }
                self::assertEquals($increase_ratio, $cnt);
            }
        }
    }

    public function testPrepareFolds()
    {
        $number_of_element = 100;
        $data = $this->generateData($number_of_element, 5);
        $expected = $this->generateExpected($number_of_element);
        $increase_ratio = 3;
        $fold_number = 10;
        $number_of_alert = array_sum($expected);
        $number_no_alert = $number_of_element - $number_of_alert;

        $ds = new TrainingDataset($data, $expected);
        $ds = $ds->increaseTrueAlert($increase_ratio);
        $folds = $ds->prepareFolds($fold_number);
        self::assertEquals($fold_number, count($folds));
        for ($i = 0; $i < count($folds); $i++) {
            echo $i;
            self::assertTrue($folds[$i]->length
                == round(($number_of_alert * $increase_ratio + $number_no_alert) / $fold_number)
            || $folds[$i]->length == round(($number_of_alert * $increase_ratio + $number_no_alert) / $fold_number) - 1);
            self::assertTrue(array_sum($folds[$i]->expected)
                == round((($number_of_alert * $increase_ratio) / $fold_number))
            || array_sum($folds[$i]->expected) == 1 + round((($number_of_alert * $increase_ratio) / $fold_number))
            || array_sum($folds[$i]->expected) == 2 + round((($number_of_alert * $increase_ratio) / $fold_number)));
        }
    }

    private function generateData(int $size, int $weights) : array
    {
        srand(5489);
        $data = [];
        for ($i = 0; $i < $size; $i++) {
            $vector = [];
            for ($j = 0; $j < $weights; $j++) {
                $vector[] = Utils::getRandomDouble();
            }
            $data[] = $vector;
        }
        return $data;
    }

    private function generateExpected(int $size) : array
    {
        srand(5768);
        $expected = [];
        for ($i = 0; $i < $size; $i++) {
            if (Utils::getRandomDouble() <= 0.5) {
                $expected[] = 0.0;
            } else {
                $expected[] = 1.0;
            }
        }
        return $expected;
    }
}
