<?php

namespace RUCD\Training;

use \Monolog\Logger;
use \PHPUnit\Framework\TestCase;
use \Monolog\Handler\StreamHandler;
use \Monolog\Formatter\LineFormatter;

class TrainerTest extends TestCase
{

    /**
     * @var Trainer
     */
    protected $trainer;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {

        $logger = new Logger("TEST");
        $streamHandler = new StreamHandler(__DIR__ . "/../../logs/" . date("Y-m-d") . ".log", Logger::INFO);
        $output = "%message% %context%\n";
        $formatter = new LineFormatter($output);
        $streamHandler->setFormatter($formatter);
        $logger->pushHandler($streamHandler);

        $parameters = new TrainerParameters($logger);

        $this->trainer = new Trainer($parameters, new SolutionDistance(5));
    }


    public function testGetRandomDouble()
    {
        $val = $this->trainer->getRandomDouble();
        $this->assertTrue($val >= 0.0);
        $this->assertTrue($val <= 1.0);
    }

    public function testGenerateInitialPopulation()
    {
        $population = $this->trainer->generateInitialPopulation(5, 10);
        $this->assertEquals(10, count($population));
        foreach ($population as $solution) {
            $this->assertEquals(5, count($solution->getWeightsP()));
            $this->assertEquals(5, count($solution->getWeightsW()));
            foreach ($solution->getWeightsP() as $weight) {
                $this->assertGreaterThanOrEqual(0, $weight);
                $this->assertLessThanOrEqual(1, $weight);
            }
            foreach ($solution->getWeightsW() as $weight) {
                $this->assertGreaterThanOrEqual(0, $weight);
                $this->assertLessThanOrEqual(1, $weight);
            }
        }
    }

    public function testSelectParentsForDistanceSolution()
    {
        // selectParents relies on array_merge() which I don't trust...
        $trainer = new Trainer(new TrainerParameters(), new SolutionDistance(10));
        $solutions = unserialize(file_get_contents(__DIR__ . "/../solutionsDistance.ser"));

        $best_solution = $trainer->findBestSolution($solutions);

        $parents = $trainer->selectParents(
            $solutions,
            30,
            TrainerParameters::SELECTION_METHOD_RWS
        );

        $this->assertEquals(30, count($parents));

        // First solution in the parents should be the best one...
        $this->assertEquals($best_solution, $parents[0]);
    }

    public function testSelectParentsForAUCSolution()
    {
        // selectParents relies on array_merge() which I don't trust...
        $trainer = new Trainer(new TrainerParameters(), new SolutionDistance(10));
        $solutions = unserialize(file_get_contents(__DIR__ . "/../solutionsAUC.ser"));

        $best_solution = $trainer->findBestSolution($solutions);

        $parents = $trainer->selectParents(
            $solutions,
            30,
            TrainerParameters::SELECTION_METHOD_RWS
        );

        $this->assertEquals(30, count($parents));

        // First solution in the parents should be the best one...
        $this->assertEquals($best_solution, $parents[0]);
    }


    public function testRunLarge()
    {
        srand(155);

        $data = $this->getData(100);
        $expected = $this->getExpectedValues(100);

        $trainer = new Trainer(new TrainerParameters(
            null,
            10,
            60,
            7,
            TrainerParameters::SELECTION_METHOD_RWS,
            20,
            TrainerParameters::INITIAL_POPULATION_GENERATION_QUASI_RANDOM
        ), new SolutionDistance(10));

        $this->assertEquals(
            7256279470.16489,
            $trainer->run($data, $expected)->getDistance() //Quasi Random initialisation
        );
    }

    public function getData($size)
    {
        return array_slice(
            unserialize(file_get_contents(__DIR__ . "/../data.ser")),
            0,
            $size
        );
    }

    public function getExpectedValues($size)
    {
        return array_slice(
            unserialize(file_get_contents(__DIR__ . "/../expected.ser")),
            0,
            $size
        );
    }


    public function testRunTOS()
    {
        // By setting the initial seed of the random number generator,
        // we can be sure the result will be the same for each execution
        srand(1234);

        $logger = new Logger('wowa-training-test');
        $logger->pushHandler(new StreamHandler('php://stdout', Logger::DEBUG));

        $trainer = new Trainer(
            new TrainerParameters(
                $logger,
                10,
                60,
                10,
                TrainerParameters::SELECTION_METHOD_TOS,
                50
            ),
            new SolutionDistance(3)
        );

        $data = [
            [0.1, 0.2, 0.3],
            [0.5, 0.3, 0.2],
            [0.6, 0.4, 0.3],
            [0.2, 0.8, 0.5],
            [0.5, 0.5, 0.6]];
        $values = [1, 2, 3, 4, 5];
        $solution = $trainer->run($data, $values);

        //$this->assertEquals(6.223301945810205, $solution->distance); //Random initialisation
        $this->assertEquals(6.2425584544769315, $solution->getDistance()); // QuasiRandomInitialisation
    }

    public function testRunRWS()
    {

        // By setting the initial seed of the random number generator,
        // we can be sure the result will be the same for each execution
        srand(1234);

        $logger = new Logger('wowa-training-testRWS');
        $logger->pushHandler(new StreamHandler('php://stdout', Logger::DEBUG));

        $trainer = new Trainer(
            new TrainerParameters(
                $logger,
                10,
                60,
                10,
                TrainerParameters::SELECTION_METHOD_RWS,
                50
            ),
            new SolutionDistance(3)
        );

        $data = [
            [0.1, 0.2, 0.3],
            [0.5, 0.3, 0.2],
            [0.6, 0.4, 0.3],
            [0.2, 0.8, 0.5],
            [0.5, 0.5, 0.6]];
        $values = [1, 2, 3, 4, 5];
        $solution = $trainer->run($data, $values);

        //$this->assertEquals(6.268799437556612, $solution->distance); // Random initialisation
        $this->assertEquals(6.234746231309783, $solution->getDistance()); //Quasi random initialisation
    }
}
